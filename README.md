# Kelompok IT11 Sistem Operasi

Kelompok IT11 :
| Nama | NRP |
| -------------------------------------- | ---------- |
| Mohammad Arkananta Radithya Taratugang | 5027221003 |
| Fazrul Ahmad Fadhilah | 5027221025 |
| Samuel Yuma Krismata | 5027221029 |

---

## Soal 1
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :
a. Membuat program C dengan nama belajar.c, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [nomor_kelompok]×2 dan matriks kedua 2×[nomor_kelompok]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. <br>
b. Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks. <br>
c. Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama yang.c. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. 
**(Catatan: wajib menerapkan konsep shared memory)** <br>
d. Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
matriks 
1	2  	3
2	2	4

maka:
1	4	6
4	4	24

**(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)** <br>
e. Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama rajin.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi. <br>
Catatan:
Key memory diambil berdasarkan program dari belajar.c sehingga program belajar.c tidak perlu menghasilkan key memory (value). Dengan demikian, pada program yang.c dan rajin.c hanya perlu mengambil key memory dari belajar.c
Untuk kelompok 1 - 9, 11 - 19, 21 menggunakan  digit akhir saja.
	Contoh : 19 -> 9
Untuk kelompok 10 dan 20 menambahkan digit awal dan akhir.
			Contoh : 20 - > 2

### Code
**belajar.c**
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int main() {
    srand(time(NULL));

    int matrix1[1][2] = { {1 + rand() % 4, 1 + rand() % 4} };
    int matrix2[2][1] = { {1 + rand() % 5}, {1 + rand() % 5} };
    int result[1][1] = { {0} };

    result[0][0] = matrix1[0][0] * matrix2[0][0] + matrix1[0][1] * matrix2[1][0];

    result[0][0]--;

    printf("Matriks Pertama:\n");
    printf("%d %d\n", matrix1[0][0], matrix1[0][1]);

    printf("Matriks Kedua:\n");
    printf("%d\n%d\n", matrix2[0][0], matrix2[1][0]);

    printf("Hasil perkalian matriks (dikurangi 1):\n");
    printf("%d\n", result[0][0]);

    key_t key = ftok("shared_memory_key", 11);

    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    *shared_result = result[0][0];

    shmdt(shared_result);

    return 0;
}

```
**yang.c**

```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h> // Sertakan header time.h

unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;

    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }

    return result;
}

struct ThreadInfo {
    int value;
    unsigned long long factorial_value;
};

void *calculateFactorial(void *arg) {
    struct ThreadInfo *info = (struct ThreadInfo *)arg;
    info->factorial_value = factorial(info->value);
    return NULL;
}

int main() {
    key_t key = ftok("shared_memory_key", 11);
    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    int transposed_result = *shared_result;

    printf("Matriks Hasil Transposisi:\n");
    printf("%d\n", transposed_result);

    pthread_t threads[1][1];

    struct ThreadInfo thread_info[1][1];

    printf("Hasil faktorial dari matriks hasil transposisi:\n");
    clock_t start_time, end_time;
    start_time = clock(); 

    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 1; j++) {
            thread_info[i][j].value = shared_result[i * 1 + j];
            pthread_create(&threads[i][j], NULL, calculateFactorial, &thread_info[i][j]);
        }
    }

    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 1; j++) {
            pthread_join(threads[i][j], NULL);
            printf("%llu ", thread_info[i][j].factorial_value); 
        }
        printf("\n");
    }

    end_time = clock(); 

    double cpu_time_used = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang dibutuhkan untuk perhitungan faktorial: %f detik\n", cpu_time_used);

    shmdt(shared_result);

    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}

```

**rajin.c**
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

unsigned long long factorial(int n) {

    if (n == 0 || n == 1)
        return 1;

    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }

    return result;
}

int main() {
    key_t key = ftok("shared_memory_key", 11);
    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    int transposed_result = *shared_result;

    printf("Matriks Hasil Transposisi:\n");
    printf("%d\n", transposed_result);

    clock_t start_time, end_time;
    start_time = clock();

    unsigned long long factorial_value = factorial(transposed_result);
    end_time = clock();


    printf("Hasil faktorial dari matriks hasil transposisi: %llu\n", factorial_value);

    double cpu_time_used = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang dibutuhkan untuk perhitungan faktorial: %f detik\n", cpu_time_used);

    shmdt(shared_result);

    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
```

### Penjelasan
**belajar.c**
Untuk membuat matriks dengan ukuran 1x2 dan 2x1 dengan isi angka random dari 1-4(matrix 1) dan 1-5(matrix 2):
```c
int matrix1[1][2] = { {1 + rand() % 4, 1 + rand() % 4} };
    int matrix2[2][1] = { {1 + rand() % 5}, {1 + rand() % 5} };
    int result[1][1] = { {0} };
```

Untuk melakukan perkalian matriks:
```c
result[0][0] = matrix1[0][0] * matrix2[0][0] + matrix1[0][1] * matrix2[1][0];
```
Untuk menampilkan matriks dan hasil perkalian matriks:
```c
printf("Matriks Pertama:\n");
    printf("%d %d\n", matrix1[0][0], matrix1[0][1]);

    printf("Matriks Kedua:\n");
    printf("%d\n%d\n", matrix2[0][0], matrix2[1][0]);

    printf("Hasil perkalian matriks (dikurangi 1):\n");
    printf("%d\n", result[0][0]);
```
Untuk mengurangi hasil perkalian dengan 1 pada setiap matriks:
```c
result[0][0]--;
```
Untuk menciptakan key:
```c
    key_t key = ftok("shared_memory_key", 11);
```
Untuk menciptakan shared memory:
```c
    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);
```
Untuk attach shared memory:
```c
    int *shared_result = (int *)shmat(shmid, NULL, 0);
```
Untuk memasukkan hasil pengurangan dari perkalian matriks ke shared memory:
```c
    *shared_result = result[0][0];
```
Untuk mendetach shared memory:
```c
    shmdt(shared_result);
```
**yang.c**
Untuk mengakses shared memory dan mengambil variabel hasil pengurangan dari perkalian matriks:
```c
key_t key = ftok("shared_memory_key", 11);
    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }
```
Untuk melakukan transpose hasil pengurangan dari perkalian matriks dan menampilkannya ke layar:
```c
    int transposed_result = *shared_result;

     printf("Matriks Hasil Transposisi:\n");
    printf("%d\n", transposed_result);

```
`pada transpose hasil pengurangan dari perkalian matriks di sini tidak ada perubahan karena hasil pengurangan dari perkalian matriks hanya berisi 1 elemen, sehingga bentuk transposenya tidak akan berbeda.`

Untuk membuat multithread dan menghitung hasil faktorial dari bentuk transpose:
```c
pthread_t threads[1][1];

    struct ThreadInfo thread_info[1][1];

    printf("Hasil faktorial dari matriks hasil transposisi:\n");
    clock_t start_time, end_time;
    start_time = clock(); 

    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 1; j++) {
            thread_info[i][j].value = shared_result[i * 1 + j];
            pthread_create(&threads[i][j], NULL, calculateFactorial, &thread_info[i][j]);
        }
    }

    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 1; j++) {
            pthread_join(threads[i][j], NULL);
            printf("%llu ", thread_info[i][j].factorial_value); 
        }
        printf("\n");
    }

    end_time = clock(); 
```
`Pada fungsi di atas saya menggunakan clock untuk menghitung waktu yang dibutuhkan untuk menghitung hasil faktorial dari bentuk transpose matriksnya`

Untuk menampilkan waktu yang dibutuhkan:
```c
double cpu_time_used = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang dibutuhkan untuk perhitungan faktorial: %f detik\n", cpu_time_used);
```

Untuk mendetach shared memory:
```c
    shmdt(shared_result);
```
Untuk menghapus shared memory setelah digunakan:
```c
    shmctl(shmid, IPC_RMID, NULL);
```

**rajin.c**
Untuk program rajin.c penjelasannya sama seperti program yang.c, yang berbeda hanyalah cara untuk menghitung nilai faktorialnya:
```c
unsigned long long factorial(int n) {

    if (n == 0 || n == 1)
        return 1;

    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }

    return result;
}
```
Program ini menghitung faktorial secara manual, tidak menggunakan thread.

Kendala untuk soal nomor 1 ini adalah program saya membutuhkan waktu yang lebih lama untuk menghitung faktorial ketika menggunakan multithread, seperti yang terlihat pada outputnya.

### Output

![output_modul_3](/uploads/80e2280774d90681c6305a61e75294f5/output_modul_3.jpg)

## Soal 2

Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di file. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : <br>
**(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).**

a. Messi ingin nama programnya 8ballondors.c. Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama thebeatles.txt <br>
b. Pada child process, lakukan perhitungan jumlah frekuensi kemunculan sebuah kata dari output file yang akan diinputkan (kata) oleh user. <br>
c. Karena gabutnya, Messi ingin program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file yang akan diinputkan (huruf) user. <br>
d. Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program :
<ol>
i. Kata	: ./8ballondors -kata <br>
ii. Huruf	: ./8ballondors -huruf
</ol>

e. Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process. <br>
f. Messi ingin agar setiap kata atau huruf dicatat dalam sebuah log yang diberi nama frekuensi.log. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process. 
<ol>
i. <b>Format:</b> [date] [type] [message] <br>
ii. <b>Type:</b> KATA, HURUF <br>
iii. <b>Ex:</b> <br>
<ol>
1. [24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt' <br>
2. [24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'
</ol>
</ol>
Catatan:
Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search.

### Code

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>

// remove non letter
void removeNonLetter(char *str)
{
    int i = 0, j = 0;
    while (str[i])
    {
        if (isalpha(str[i]) || str[i] == ' ' || str[i] == '\n')
        {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

// itung huruf
void countLetter(const char *str, const char letter, int *count)
{
    *count = 0;

    for (int i = 0; str[i] != '\0'; i++)
    {
        if (isalpha(str[i]) && str[i] == letter)
        {
            (*count)++;
        }
    }
}

// itung kata
void countWord(const char *str, const char *word, int *count)
{
    char read_word[100];
    *count = 0;
    const char *delimiter = " \n";

    char *token = strtok((char *)str, delimiter);

    while (token != NULL)
    {
        removeNonLetter(token);
        if (strcmp(word, token) == 0)
        {
            (*count)++;
        }
        token = strtok(NULL, delimiter);
    }
}

// bikin frekuensi.log
void writeLog(const char *type, const char *item, int count)
{
    FILE *log_file = fopen("frekuensi.log", "a");

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    fprintf(log_file, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n",
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec,
            type, (strcmp(type, "KATA") == 0) ? "Kata" : "Huruf", item, count);

    fclose(log_file);
}

// fungsi download
int downloadFile(const char *url, const char *outputFileName)
{
    char wgetCommand[256];
    snprintf(wgetCommand, sizeof(wgetCommand), "wget --quiet --output-document=%s 'https://drive.google.com/uc?export=download&id=%s'", outputFileName, url);

    FILE *wgetProcess = popen(wgetCommand, "r");
    if (!wgetProcess) {
        perror("Failed to run wget");
        return 1;
    }

    int status = pclose(wgetProcess);
    if (status == -1) {
        perror("pclose() failed");
        return 1;
    } else if (WEXITSTATUS(status) != 0) {
        fprintf(stderr, "wget failed with exit code %d\n", WEXITSTATUS(status));
        return 1;
    }

    return 0;
}

// fungsi main
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: ./8ballondors -kata <item> or -huruf <item>\n");
        return 1;
    }

    int word_fd[2], letter_fd[2];
    pid_t pid;

    if (pipe(word_fd) == -1 || pipe(letter_fd) == -1)
    {
        perror("Pipe creation failed");
        return 1;
    }

    pid = fork();

    if (pid < 0)
    {
        perror("Fork failed");
        return 1;
    }

    if (pid == 0)
    { // child process
        close(word_fd[0]);
        close(letter_fd[0]);

        int downloadResult = downloadFile("16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW", "lirik.txt");
        if (downloadResult != 0)
        {
            printf("Gagal mengunduh file lirik.txt\n");
            return 1;
        }

        FILE *file = fopen("lirik.txt", "r");

        if (file == NULL) {
            perror("File open failed");
            return 1;
        }

        char *file_contents = NULL;
        size_t file_size = 0;

        fseek(file, 0, SEEK_END);
        file_size = ftell(file);
        fseek(file, 0, SEEK_SET);

        file_contents = malloc(file_size + 1);
        fread(file_contents, sizeof(char), file_size, file);

        file_contents[file_size] = '\0';

        int word_count, letter_count;
        if (strcmp(argv[1], "-kata") == 0)
        {
            if (strlen(argv[2]) == 0)
            {
                printf("Salah\n");
                free(file_contents);
                return 1;
            }
            countWord(file_contents, argv[2], &word_count);
            write(word_fd[1], &word_count, sizeof(word_count));
        }
        else if (strcmp(argv[1], "-huruf") == 0)
        {
            if (strlen(argv[2]) != 1)
            {
                printf("Salah\n");
                free(file_contents);
                return 1;
            }
            countLetter(file_contents, argv[2][0], &letter_count);
            write(letter_fd[1], &letter_count, sizeof(letter_count));
        }
        else
        {
            printf("Salah\n");
            free(file_contents);
            return 1;
        }

        free(file_contents);
        close(word_fd[1]);
        close(letter_fd[1]);
        exit(0);
    }
    else
    { // parent process
        close(word_fd[1]);
        close(letter_fd[1]);

        FILE *input_file = fopen("lirik.txt", "r");
        FILE *output_file = fopen("thebeatles.txt", "w");

        char str[100];

        while (fgets(str, 100, input_file) != NULL)
        {
            removeNonLetter(str);
            fputs(str, output_file);
        }

        fclose(input_file);
        fclose(output_file);

        int word_count, letter_count;
        read(word_fd[0], &word_count, sizeof(word_count));
        read(letter_fd[0], &letter_count, sizeof(letter_count));

        const char *type = strcmp(argv[1], "-kata") == 0 ? "KATA" : "HURUF";

        writeLog(type, argv[2], type == "KATA" ? word_count : letter_count);

        close(word_fd[0]);
        close(letter_fd[0]);
    }

    return 0;
}
```

### Penjelasan

**Poin a** <br>
Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama thebeatles.txt

**Fungsi removeNonLetter**
```c
void removeNonLetter(char *str)
{
    int i = 0, j = 0;
    while (str[i])
    {
        if (isalpha(str[i]) || str[i] == ' ' || str[i] == '\n')
        {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}
```

Fungsi `removeNonLetter` menghapus karakter-karakter yang bukan huruf dari sebuah string. Ini dilakukan dengan menggunakan dua indeks, `i` dan `j`, yang melintasi string. Karakter yang merupakan huruf (`isalpha`), spasi, atau karakter baris baru (`'\n'`) akan dipindahkan ke posisi yang benar dalam string, sedangkan karakter lainnya akan diabaikan. Akhirnya, null terminator (`'\0'`) ditempatkan pada akhir string yang telah dibersihkan untuk menandakan akhir dari string yang telah dimodifikasi.

```c
FILE *input_file = fopen("lirik.txt", "r");
FILE *output_file = fopen("thebeatles.txt", "w");

char str[100];

while (fgets(str, 100, input_file) != NULL)
{
    removeNonLetter(str);
    fputs(str, output_file);
}

fclose(input_file);
fclose(output_file);
```

Kode diatas berfungsi untuk membaca teks dari file "lirik.txt", membersihkan teks dari karakter yang bukan huruf, dan kemudian menulis teks yang telah dibersihkan ke file "thebeatles.txt".

Penjelasan:
1. `FILE *input_file = fopen("lirik.txt", "r")` : Kode ini membuka file "lirik.txt" dalam mode baca ("r") dan menyimpan file tersebut dalam pointer `input_file`. Ini memungkinkan program untuk membaca isi dari "lirik.txt".

2. `FILE *output_file = fopen("thebeatles.txt", "w")` : Kode ini membuka atau menciptakan file "thebeatles.txt" dalam mode tulis ("w") dan menyimpan file tersebut dalam pointer `output_file`. Ini memungkinkan program untuk menulis isi yang telah dibersihkan ke dalam "thebeatles.txt". Jika file "thebeatles.txt" sudah ada, maka isi sebelumnya akan terhapus.

3. `char str[100]` : Ini adalah array karakter (string) dengan panjang 100 yang digunakan untuk membaca setiap baris teks dari "lirik.txt". Setiap baris akan disimpan dalam variabel `str`.

4. `while (fgets(str, 100, input_file) != NULL)` : Kode ini menggunakan loop `while` untuk membaca setiap baris dari "lirik.txt" menggunakan fungsi `fgets`. Fungsi `fgets` membaca setiap baris (maksimal 100 karakter per baris) dari `input_file` dan menyimpannya dalam variabel `str`. Loop akan berlanjut selama masih ada baris yang dapat dibaca dari file.

5. `removeNonLetter(str)` : Setiap baris teks yang telah dibaca dari "lirik.txt" diproses dengan memanggil fungsi `removeNonLetter`. Fungsi ini akan menghapus karakter-karakter yang bukan huruf dari teks yang dibaca.

6. `fputs(str, output_file)` : Setiap baris teks yang telah dibersihkan dari karakter yang bukan huruf akan ditulis ke file "thebeatles.txt" menggunakan fungsi `fputs`. Ini akan menghasilkan file "thebeatles.txt" yang berisi teks yang telah dibersihkan.

7. `fclose(input_file)` : Setelah selesai membaca semua baris dari "lirik.txt" dan menulis ke "thebeatles.txt", file "lirik.txt" ditutup dengan fungsi `fclose` agar tidak ada lagi operasi pembacaan pada file tersebut.

8. `fclose(output_file)` : File "thebeatles.txt" juga ditutup dengan fungsi `fclose` setelah selesai menulis semua teks yang telah dibersihkan. Dengan menutup file, perubahan akan tersimpan dan file tersebut dapat digunakan atau dibaca oleh program lain.

**Poin b,c, dan d** <br>
Melakukan perhitungan jumlah frekuensi kemunculan sebuah kata atau huruf dari output file yang akan diinputkan (kata) atau (huruf) oleh user <br>

Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program :

i. Kata	: ./8ballondors -kata <br>
ii. Huruf	: ./8ballondors -huruf

**Fungsi countWord**
```c
void countWord(const char *str, const char *word, int *count)
{
    char read_word[100];
    *count = 0;
    const char *delimiter = " \n";

    char *token = strtok((char *)str, delimiter);

    while (token != NULL)
    {
        removeNonLetter(token);
        if (strcmp(word, token) == 0)
        {
            (*count)++;
        }
        token = strtok(NULL, delimiter);
    }
}
```

Fungsi `countWord` ini akan menghitung berapa kali kata yang dicari oleh pengguna muncul dalam teks yang diinputkan (dalam parameter `str`). Hasil perhitungan frekuensi kemudian disimpan dalam variabel `count`, yang akan digunakan untuk menampilkan hasil dalam log dan pada akhirnya dalam file `frekuensi.log`.

**Fungsi countLetter**

```c
void countLetter(const char *str, const char letter, int *count)
{
    *count = 0;

    for (int i = 0; str[i] != '\0'; i++)
    {
        if (isalpha(str[i]) && str[i] == letter)
        {
            (*count)++;
        }
    }
}
```

Fungsi `countLetter` ini menerima teks (`str`) dan huruf (`letter`) yang ingin dihitung kemunculannya. Saat berjalan, fungsi ini memeriksa setiap karakter dalam teks, dan jika karakter tersebut adalah huruf (diperiksa menggunakan `isalpha`) dan sama dengan huruf yang dicari, maka variabel `count` akan ditambahkan. Hasil perhitungan disimpan dalam variabel `count`, yang akan digunakan untuk menampilkan hasil dalam log.

**Fungsi main** <br>
Pada fungsi `main`, perhitungan jumlah frekuensi kemunculan sebuah kata atau huruf terjadi dalam blok kode berikut:
```c
int word_count, letter_count;
if (strcmp(argv[1], "-kata") == 0)
{
    if (strlen(argv[2]) == 0)
    {
        printf("Salah\n");
        free(file_contents);
        return 1;
    }
    countWord(file_contents, argv[2], &word_count);
    write(word_fd[1], &word_count, sizeof(word_count));
}
else if (strcmp(argv[1], "-huruf") == 0)
{
    if (strlen(argv[2]) != 1)
    {
        printf("Salah\n");
        free(file_contents);
        return 1;
    }
    countLetter(file_contents, argv[2][0], &letter_count);
    write(letter_fd[1], &letter_count, sizeof(letter_count));
}
```

Di sini, terdapat dua kondisi yang memeriksa apakah pengguna ingin menghitung frekuensi kata atau frekuensi huruf. Jika pengguna memilih `-kata`, maka fungsi `countWord` dipanggil untuk menghitung jumlah kemunculan kata yang diinputkan oleh pengguna. Jika pengguna memilih `-huruf`, maka fungsi `countLetter` dipanggil untuk menghitung jumlah kemunculan huruf yang diinputkan oleh pengguna. Hasil perhitungan frekuensi disimpan dalam variabel `word_count` atau `letter_count` dan kemudian ditulis ke dalam pipe untuk dikirim ke proses induk.

**Poin e**

Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process

**Fungsi writeLog**
```c
void writeLog(const char *type, const char *item, int count)
{
    FILE *log_file = fopen("frekuensi.log", "a");

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    fprintf(log_file, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n",
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec,
            type, (strcmp(type, "KATA") == 0) ? "Kata" : "Huruf", item, count);

    fclose(log_file);
}
```
Fungsi `writeLog` ini membuka file "frekuensi.log" untuk ditambahkan (`"a"`), lalu mencetak informasi mengenai perhitungan frekuensi ke dalam file tersebut, termasuk tanggal dan waktu, jenis perhitungan (kata atau huruf), item yang dihitung, dan jumlah kemunculannya dalam file "thebeatles.txt".

**Fungsi main**
```c
int word_count, letter_count;
if (strcmp(argv[1], "-kata") == 0)
{
    if (strlen(argv[2]) == 0)
    {
        printf("Salah\n");
        free(file_contents);
        return 1;
    }
    countWord(file_contents, argv[2], &word_count);
    write(word_fd[1], &word_count, sizeof(word_count));
}
else if (strcmp(argv[1], "-huruf") == 0)
{
    if (strlen(argv[2]) != 1)
    {
        printf("Salah\n");
        free(file_contents);
        return 1;
    }
    countLetter(file_contents, argv[2][0], &letter_count);
    write(letter_fd[1], &letter_count, sizeof(letter_count));
}
```
Dalam kondisi ini, setelah perhitungan selesai, hasil perhitungan disimpan dalam variabel `word_count` atau `letter_count`, dan kemudian hasil tersebut ditulis ke dalam pipe `word_fd[1]` atau `letter_fd[1]`. Ini adalah cara untuk mengirim hasil perhitungan ke parent process yang berjalan di sampingnya, sehingga parent process dapat membaca hasil tersebut dari pipe. Hasil ini kemudian digunakan untuk mencatat informasi ke dalam file log "frekuensi.log".

**Poin f**

Bagian yang menjawab permintaan untuk mencatat setiap kata atau huruf dalam file log yang diberi nama "frekuensi.log" terletak dalam blok kode berikut di dalam fungsi `main` pada parent process:

```c
const char *type = strcmp(argv[1], "-kata") == 0 ? "KATA" : "HURUF";

writeLog(type, argv[2], type == "KATA" ? word_count : letter_count);
```

Di sini, program menggunakan fungsi `writeLog` untuk mencatat informasi tentang jenis perhitungan (kata atau huruf), item yang dihitung (argumen ke-2), dan jumlah kemunculan dalam file "thebeatles.txt" berdasarkan hasil yang dikirim dari child process. Ini adalah bagian kode yang menghasilkan file log "frekuensi.log" berdasarkan data yang dikirim dari child process.

**Output**

**Isi lirik.txt** <br>
![image](/uploads/7f76d2214577296b61ae647c244ae728/image.png)

**Isi thebeatles.txt** <br>
![image](/uploads/d8e3a97a0d5b0b7b455cfa02377891d6/image.png)

**Command untuk menjalankan 8ballondors** <br>
![image](/uploads/372504605a654803440acd351299fb31/image.png)

**Isi frekuensi.log** <br>
![image](/uploads/b822357fe09612d583bb805a78f3f048/image.png)






## Soal 3

Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut : 

a. Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep **message queue (wajib)** maka buatlah 2  program, **sender.c** sebagai pengirim dan **receiver.c** sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.

```
└── soal3
	├── users
	├── receiver.c
	└── sender.c
```

b. Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah **CREDS** kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver.

```
./receiver
Username: Mayuri, Password: TuTuRuuu
Username: Onodera, Password: K0sak!
Username: Johan, Password: L!3b3rt
Username: Seki, Password: Yuk!n3
Username: Ayanokouji, Password: K!yot4kA
. . .
```

c. Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah **AUTH: username password** kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.

d. Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah **TRANSFER filename**. 

```
└── soal3
       ├── Receiver
       ├── Sender
       ├── receiver.c
       └── sender.c
```

e. Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.

f. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan **"UNKNOWN COMMAND"**.

Catatan:
- Dilarang untuk overwrite file users secara permanen
- Ketika proses autentikasi gagal program receiver akan mengirim status Authentication failed dan program langsung keluar
- Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu
- Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.

### Code

File **receiver.c**

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define MAX_MESSAGE_SIZE 100
#define MESSAGE_TYPE 1

struct pesan {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

void decodeBase64(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    int length = BIO_read(bio, output, strlen(input));
    output[length] = '\0';

    BIO_free_all(bio);
}

void showCredentials(FILE *file, FILE *outputFile) {
    char line[MAX_MESSAGE_SIZE];
    while (fgets(line, sizeof(line), file)) {
        char *username = strtok(line, ":");
        char *encryptedPassword = strtok(NULL, "\n");

        char decryptedPassword[MAX_MESSAGE_SIZE];
        decodeBase64(encryptedPassword, decryptedPassword);

        printf("Username: %s, Password: %s\n", username, decryptedPassword);

        fprintf(outputFile, "Username: %s, Password: %s\n", username, decryptedPassword);
    }
}

int authUser(const char *username, const char *password, FILE *file) {
    char line[MAX_MESSAGE_SIZE];
    char storedUsername[MAX_MESSAGE_SIZE];
    char storedDecryptedPassword[MAX_MESSAGE_SIZE];

    while (fgets(line, sizeof(line), file)) {
        sscanf(line, "Username: %[^,], Password: %[^\n]", storedUsername, storedDecryptedPassword);

        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedDecryptedPassword) == 0) {
            return 1;
        }
    }

    return 0;
}

int main() {
    key_t key = ftok(".", 'A');
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    struct pesan msg;

    while (1) {
        if (msgrcv(msgid, &msg, sizeof(msg.mtext), MESSAGE_TYPE, 0) == -1) {
            perror("msgrcv");
            exit(EXIT_FAILURE);
        }

        if (strncmp(msg.mtext, "AUTH:", 5) == 0) {
            char *authCommand = msg.mtext + 5;

            while (*authCommand == ' ') {
                ++authCommand;
            }

            char *username = strtok(authCommand, " ");
            char *password = strtok(NULL, "\n");

            if (password != NULL) {
                char *end = password + strlen(password) - 1;
                while (*end == ' ') {
                    *end-- = '\0';
                }
            }

            FILE *file = fopen("users/decrypted.txt", "r");
            if (!file) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }

            if (authUser(username, password, file)) {
                printf("\nAuthentication successful\n");
            } else {
                printf("\nAuthentication failed\n");
                fclose(file);
                break;
            }

            fclose(file);
        } else if (strcmp(msg.mtext, "CREDS\n") == 0) {
            FILE *file = fopen("users/users.txt", "r");
            if (!file) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }

            FILE *outputFile = fopen("users/decrypted.txt", "w");
            if (!outputFile) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }

            showCredentials(file, outputFile);

            fclose(file);
            fclose(outputFile);
        }
    }

    return 0;
}
```

File **sender.c**

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

#define MAX_MESSAGE_SIZE 100
#define MESSAGE_TYPE 1

struct pesan {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

int main() {
    key_t key = ftok(".", 'A');
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    struct pesan msg;
    msg.mtype = MESSAGE_TYPE;

    printf("Masukkan perintah: ");
    fgets(msg.mtext, sizeof(msg.mtext), stdin);

    if (msgsnd(msgid, &msg, sizeof(msg.mtext), 0) == -1) {
        perror("msgsnd");
        exit(EXIT_FAILURE);
    }

    return 0;
}
```

### Penjelasan

**File receiver.c**

``` c
void decodeBase64(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    int length = BIO_read(bio, output, strlen(input));
    output[length] = '\0';

    BIO_free_all(bio);
}
```
1. Fungsi yang digunakan untuk mengambil teks yang dienkripsi menggunakan base64, mendekodekannya, dan menyimpan hasilnya dalam sebuah string. Prosesnya dimulai dari inisiasi dua objek `BIO` yaitu `bio` dan `b64`. Selanjutnya, dilakukan konfigurasi terhadap objek tersebut dengan membuat objek `BIO` untuk base64, membuat objek `BIO` dari input, serta menggabungkan objek `BIO` dan menjadikan `bio` sebagai objek yang melakukan base64 decoding. Lalu, dilakukan dekode base64 yaitu dengan membaca dan mendekode teks base64 ke dalam `output`. Terakhir, ditambahkan null terminator untuk menghilangkan karakter yang tidak perlu ditambahkan serta ditambahkan juga fungsi untuk membersihkan objek `BIO` yaitu `BIO_free_all(bio)`.

``` c
void showCredentials(FILE *file, FILE *outputFile) {
    char line[MAX_MESSAGE_SIZE];
    while (fgets(line, sizeof(line), file)) {
        char *username = strtok(line, ":");
        char *encryptedPassword = strtok(NULL, "\n");

        char decryptedPassword[MAX_MESSAGE_SIZE];
        decodeBase64(encryptedPassword, decryptedPassword);

        printf("Username: %s, Password: %s\n", username, decryptedPassword);

        fprintf(outputFile, "Username: %s, Password: %s\n", username, decryptedPassword);
    }
}
```
2. Fungsi yang digunakan untuk membaca file yang berisi informasi credentials (username dan password terenkripsi), mendekripsi password, dan menampilkan informasi ke layar serta menuliskannya ke dalam sebuah file. Prosesnya dimulai dari deklarasi array yang digunakan untuk menyimpan setiap baris dari file yang berisi credentials. Kemudian, digunakan sebuah looping dengan perintah `fgets` untuk membaca satu baris dalam file credentials. Selanjutnya, digunakan perintah `strtok` untuk memisahkan baris menjadi dua bagian berdasarkan delimiter ":". `username` menjadi bagian sebelum ":", dan `encryptedPassword` menjadi bagian setelahnya hingga karakter newline ("\n"). Lalu, digunakan fungsi yang telah dibuat sebelumnya, yaitu fungsi `decodeBase64` untuk mendekripsi password yang terenkripsi dan menyimpannya dalam variabel `decryptedPassword`. Terakhir, informasi credentials yang berhasil diperoleh ditampilkan ke layar dan disimpan ke dalam file output (`decrypted.txt`).

``` c
int authUser(const char *username, const char *password, FILE *file) {
    char line[MAX_MESSAGE_SIZE];
    char storedUsername[MAX_MESSAGE_SIZE];
    char storedDecryptedPassword[MAX_MESSAGE_SIZE];

    while (fgets(line, sizeof(line), file)) {
        sscanf(line, "Username: %[^,], Password: %[^\n]", storedUsername, storedDecryptedPassword);

        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedDecryptedPassword) == 0) {
            return 1;
        }
    }

    return 0;
}
```
3. Fungsi yang digunakan untuk memverifikasi apakah kombinasi username dan password yang diberikan sesuai dengan yang ada dalam file credentials (`decrypted.txt`). Prosesnya dimulai dari deklarasi array karakter untuk menyimpan informasi dari setiap baris file. Selanjutnya, digunakan sebuah looping dengan perintah `fgets` untuk membaca setiap satu baris dalam file credentials dan digunakan juga perintah `sscanf` untuk memproses baris dan mengambil value dari setiap baris. Format tersebut sesuai dengan format yang diharapkan, yaitu `"Username: ... , Password: ..."`, dan nilai username dan password terdekripsi diambil dan disimpan ke dalam variabel `storedUsername` dan `storedDecryptedPassword`. Terakhir, terdapat kondisi yang akan mengecek apakah kombinasi username dan password yang diberikan dengan kombinasi yang ada di file cocok atau tidak, yang mana apabila cocok akan mereturn 1 yang mendakan bahwa proses autentikasi berhasil.

``` c
int main() {
    key_t key = ftok(".", 'A');
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    struct pesan msg;

    while (1) {
        if (msgrcv(msgid, &msg, sizeof(msg.mtext), MESSAGE_TYPE, 0) == -1) {
            perror("msgrcv");
            exit(EXIT_FAILURE);
        }

        // kondsi if-else "AUTH:"
        // kondisi if-else "CREDS"
    }

    return 0;
}
```
4. Fungsi inti yang berisi implementasi sederhana dari server autentikasi menggunakan message queues. Prosesnya dimulai dari membuat key untuk message queue dengan menggunakan perintah `ftok` dan mengakses message queue menggunakan perintah `msget` yang mana apabila message queue belum ada, maka akan dibuat dengan menggunakan perintah `IPC_CREAT`. Kemudian, terdapat kondisi yang akan memeriksa apakah operasi `msgget` berhasil dijalankan atau tidak yang mana apabila tidak berhasil dijalankan maka akan ditampilkan pesan error dan keluar dari program. Selanjutnya, dijalankan looping agar program terus menerus berjalan dan ditambahkan perintah `msgrcv` untuk menerima pesan dari file `sender.c`. Apabila pesan gagal diterima, maka akan ditampilkan pesan error dan keluar dari program.

``` c
// kondsi if-else "AUTH:"
if (strncmp(msg.mtext, "AUTH:", 5) == 0) {
    char *authCommand = msg.mtext + 5;

    while (*authCommand == ' ') {
        ++authCommand;
    }

    char *username = strtok(authCommand, " ");
    char *password = strtok(NULL, "\n");

    if (password != NULL) {
        char *end = password + strlen(password) - 1;
        while (*end == ' ') {
            *end-- = '\0';
        }
    }

    FILE *file = fopen("users/decrypted.txt", "r");
    if (!file) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    if (authUser(username, password, file)) {
        printf("\nAuthentication successful\n");
    } else {
        printf("\nAuthentication failed\n");
        fclose(file);
        break;
    }

    fclose(file);
}
```
5. Kondisi yang digunakan untuk memeriksa apakah isi pesan dimulai dengan `AUTH:` atau tidak. Jika iya, maka program akan mengeksekusi variabel pointer `authCommand` akan dipindahkan ke lokasi setelah "AUTH:" dan mengabaikan spasi. Selanjutnya, digunakan perintah `strtok` untuk mengambil username dan password dari pesan yang diterima. Kemudian, file `decrypted.txt` yang ada dalam folder `users` dibuka untuk diperiksa credentialsnya menggunakan fungsi `authUser` yang mana jika prosesnya berhasil, maka akan ditampilkan pesan "Authentication successful" dan apabila gagal akan ditampilkan pesan "Authentication failed" lalu program `receiver` akan langsung berhenti dan keluar.

``` c
// kondisi if-else "CREDS"
else if (strcmp(msg.mtext, "CREDS\n") == 0) {
    FILE *file = fopen("users/users.txt", "r");
    if (!file) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    FILE *outputFile = fopen("users/decrypted.txt", "w");
    if (!outputFile) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    showCredentials(file, outputFile);

    fclose(file);
    fclose(outputFile);
}
```
6. Kondisi yang digunakan untuk memeriksa apakah isi pesan dimulai dengan `CREDS` atau tidak. Jika iya, maka program akan mengeksekusi untuk membuka file `users.txt` dan membuat file `decrypted.txt` di dalam folder `users`. Selanjutnya, fungsi `showCredentials` dipanggil untuk menampilkan dan menyimpan credentials yang sudah terdekripsi ke file `decrypted.txt` tersebut.

**File sender.c** <br> 

``` c
#define MAX_MESSAGE_SIZE 100
#define MESSAGE_TYPE 1

struct pesan {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};
```
1. Deklarasi makro untuk ukuran maksimum dari sebuah pesan dan jenis pesan serta deklarasi sebuah struct yang digunakan untuk menyimpan informasi tentang sebuah pesan, termasuk jenis (`mtype`) dan teksnya (`mtext`).

``` c
key_t key = ftok(".", 'A');
int msgid = msgget(key, 0666 | IPC_CREAT);
```
2. Membuat `key` untuk antrean pesan menggunakan perintah `ftok`. Selanjutnya, perintah `mget` digunakan untuk mendapatkan ID antrean pesan atau membuat antrean pesan baru jika belum ada ID. ID antrean pesan ini kemudian digunakan untuk mengirim dan menerima pesan.

```c
if (msgid == -1) {
    perror("msgget");
    exit(EXIT_FAILURE);
}
```
3. Kondisi yang mengecek jika terdapat kesalahan saat mendapatkan atau membuat antran pesan, maka program akan menampilkan pesan error menggunakan perintah `perror` dan keluar dari program.

``` c
struct pesan msg;
msg.mtype = MESSAGE_TYPE;
```
4. Membuat sebuah variabel `msg` dari struct `pesan` dan jenis pesan (`mtype`) diatur sesuai dengan nilai yang didefinisikan oleh makro `MESSAGE_TYPE`.

``` c
printf("Masukkan perintah: ");
fgets(msg.mtext, sizeof(msg.mtext), stdin);
```
5. Meminta user untuk memasukkan sebuah perintah ke dalam program lalu menyimpannya dalam sebuah array `mtext` dari `struct msg`.

``` c
if (msgsnd(msgid, &msg, sizeof(msg.mtext), 0) == -1) {
    perror("msgsnd");
    exit(EXIT_FAILURE);
}
```
6. Perintah `msgnd` digunakan untuk mengirim pesan ke antrean pesan yang mana apabila gagal maka program akan menampilkan pesan kesalahan dan keluar dari program.

### Output

**File `users.txt` & `decrypted.txt` dalam folder `users`** <br>

![users___decrypted](/uploads/ef90694c4388404ed41daab302973a2c/users___decrypted.png)

**Output perintah `CREDS`** <br>

![CREDS](/uploads/497230196ba227f1f8b61c1231ea655a/CREDS.png)

**Output perintah `AUTH: username password` (kondisi benar)** <br>

![AUTH_benar](/uploads/0a72ec4d7b846dd3885b61218a2cbd8a/AUTH_benar.png)

**Output perintah `AUTH: username password` (kondisi salah)** <br>

![AUTH_salah](/uploads/a21e2d767f14ae60abd30a88f33f9445/AUTH_salah.png)

## Soal 4

Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut: <br>

a. Client dan server terhubung melalui socket. <br>

b. Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja. <br>

c. Karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja. 

### Code 

**File `server.c`**

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>

#define PORT 5000
#define MAX_CLIENTS 5

void *clientHandler(void *socketDescription) {
    int *klien = (int *)socketDescription;
    char nama[128];
    char pesan[4096];

    recv(*klien, nama, sizeof(nama), 0);

    while (1) {
        if (recv(*klien, pesan, sizeof(pesan), 0) <= 0) {
            printf("\nKoneksi dengan klien '%s' terputus!\n\n", nama);
            break;
        }

        printf("%s: %s", nama, pesan);
        memset(pesan, 0, sizeof(pesan));
    }

    close(*klien);
    free(klien);
    pthread_exit(NULL);
}

int main() {
    int server_fd, newSocket, c;
    struct sockaddr_in serverAddress, clientAddress;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Terdapat kesalahan dalam membuat socket.");
        exit(EXIT_FAILURE);
    }

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Proses bind gagal.");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, MAX_CLIENTS) < 0) {
        perror("Terdapat kesalahan saat menghubungkan server dengan klien.");
        exit(EXIT_FAILURE);
    }

    printf("Server terhubung dengan port %d\n", PORT);
    printf("Menunggu klien terhubung dengan server.\n\n");

    c = sizeof(struct sockaddr_in);

    pthread_t client_threads[MAX_CLIENTS];

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        if ((newSocket = accept(server_fd, (struct sockaddr *)&clientAddress, (socklen_t *)&c))) {
            int *clientSocket = malloc(sizeof(int));
            *clientSocket = newSocket;

            if (pthread_create(&client_threads[i], NULL, clientHandler, (void *)clientSocket) < 0) {
                perror("Tidak dapat membuat thread");
                return 1;
            }
        } else {
            perror("Gagal menghubungkan server dengan klien.");
            return 1;
        }
    }

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        pthread_join(client_threads[i], NULL);
    }

    close(server_fd);
    return 0;
}
```

**File `client.c`**

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 5000

int main() {
    int sock = 0;
    struct sockaddr_in serverAddress;
    char nama[128];
    char pesan[4096];

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Terdapat kesalahan saat membuat socket.");
        exit(EXIT_FAILURE);
    }

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0) {
        perror("Alamat IP salah / tidak didukung.");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Gagal terhubung dengan server.");
        exit(EXIT_FAILURE);
    }

    printf("Masukkan nama: ");
    fgets(nama, sizeof(nama), stdin);
    nama[strcspn(nama, "\n")] = 0;

    if (send(sock, nama, sizeof(nama), 0) == -1) {
        perror("Gagal mengirim 'nama' ke server");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("Masukkan pesan: ");
        fgets(pesan, sizeof(pesan), stdin);

        if (strcmp(pesan, "keluar\n") == 0) {
            break;
        }

        if (send(sock, pesan, sizeof(pesan), 0) == -1) {
            perror("Gagal mengirim 'pesan' ke server");
            exit(EXIT_FAILURE);
        }
    }

    close(sock);
    return 0;
}
```

### Penjelasan

**File `server.c`**

``` c
void *clientHandler(void *socketDescription) {
    int *klien = (int *)socketDescription;
    char nama[128];
    char pesan[4096];

    recv(*klien, nama, sizeof(nama), 0);

    while (1) {
        if (recv(*klien, pesan, sizeof(pesan), 0) <= 0) {
            printf("\nKoneksi dengan klien '%s' terputus!\n\n", nama);
            break;
        }

        printf("%s: %s", nama, pesan);
        memset(pesan, 0, sizeof(pesan));
    }

    close(*klien);
    free(klien);
    pthread_exit(NULL);
}
```
1. Fungsi yang dieksekusi oleh setiap thread untuk menangani koneksi dengan klien (file `client`), yaitu menerima nama dan pesan dari klien, menampilkan pesan dan nama tersebut pada konsol, serta menutup koneksi dengan klien apabila setelah koneksi terputus. Prosesnya diawali dengan mengonversi deskripsi soket yang diterima sebagai parameter menjadi tipe data pointer integer agar deskripsi soket tersebut dapat digunakan dalam fungsi-fungsi yang memerlukan pointer ke integer. Selanjutnya, digunakan perintah `recv` untuk menerima data berupa nama dari klien lalu menyimpannya dalam variabel `nama`. Kemudian, terdapat loop yang digunakan untuk terus menerus menerima pesan dari klien dan ditambah dengan sebuah kondisi yang mengecek apabila perintah `recv` mereturn nilai kurang dari atau sama dengan 0, maka artinya koneksi dengan klien terputus, dan server akan keluar dari loop. Sebaliknya, apabila server mencetak pesan yang diterima dengan nama klien ke konsol dan mengosongkan isi dari variabel `pesan` menggunakan perintah `memset`. Terakhir, setelah loop selesai dijalankan, fungsi akan menutup koneksi dengan klien menggunakan perintah `close`, mengosongkan memori dengan perintah `free` dan keluar dari thread.

2. `server_fd = socket(AF_INET, SOCK_STREAM, 0)` : Membuat soket dengan domain `AF_INET (IPv4)`, tipe `SOCK_STREAM (TCP)` dan protokol 0 (default)

3. `bind(server_fd, (struct sockaddr *)&serverAddress, sizeof(serverAddress))` : Mengikat (`bind`) soket server ke alamat dan port yang telah ditentukan, yaitu port 5000 (sesuai dengan makro yang telah dideklarasikan)

4. `listen(server_fd, MAX_CLIENTS)` : Menetapkan server untuk mendengarkan koneksi dari klien, dengan batasan jumlah maksimum klien

5. `newSocket = accept(server_fd, (struct sockaddr *)&clientAddress, (socklen_t *)&c)` : Menerima koneksi dari klien dan membuat soket baru untuk berkomunikasi dengan klien tersebut.

6. `pthread_create(&client_threads[i], NULL, clientHandler, (void *)clientSocket)` : Membuat thread baru untuk menangani setiap koneksi dengan memanggil fungsi `clientHandler`

7. `pthread_join(client_threads[i], NULL)` : Menunggu setiap thread yang menangani koneksi selesai dijalankan

8. `close(server_fd)` : Menutup soket server

**File `client.c`**

``` c
int sock = 0;
struct sockaddr_in serverAddress;
char nama[128];
char pesan[4096];
```
1. Deklarasi variabel yang digunakan meliputi soket, alamat server, nama dan pesan

``` c
if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("Terdapat kesalahan saat membuat socket.");
    exit(EXIT_FAILURE);
}
```
2. Membuat soket menggunakan perintah `socket` yang mana jika terdapat kesalahan dalam pembuatan soket tersebut, maka program akan keluar dengan pesan error

``` c
serverAddress.sin_family = AF_INET;
serverAddress.sin_port = htons(PORT);

if (inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0) {
    perror("Alamat IP salah / tidak didukung.");
    exit(EXIT_FAILURE);
}
```
3. Melakukan konfigurasi terhadap alamat server dengan mengisi struct `sockaddr_in` dengan informasi alamat server yang meliputi tipe alamat, port, dan IP address

``` c
if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
    perror("Gagal terhubung dengan server.");
    exit(EXIT_FAILURE);
}
```
4. Menghubungkan ke server dengan perintah `connect` yang mana jika gagal terhubung dengan server, maka program akan keluar dengan pesan error

``` c
printf("Masukkan nama: ");
fgets(nama, sizeof(nama), stdin);
nama[strcspn(nama, "\n")] = 0;

if (send(sock, nama, sizeof(nama), 0) == -1) {
    perror("Gagal mengirim 'nama' ke server");
    exit(EXIT_FAILURE);
}
```
5. Meminta input dari pengguna yang kemudian disimpan dalam variabel `nama` dan selanjutnya dikirim ke server yang mana apabila terdapat kegagalan dalam proses mengirim tersebut, program akan keluar dan pesan error ditampilkan. Dalam baris kode tersebut, ditambahkan fungsi untuk membersihkan karakter newline agar ...

``` c
while (1) {
    printf("Masukkan pesan: ");
    fgets(pesan, sizeof(pesan), stdin);

    if (strcmp(pesan, "keluar\n") == 0) {
        break;
    }

    if (send(sock, pesan, sizeof(pesan), 0) == -1) {
        perror("Gagal mengirim 'pesan' ke server");
        exit(EXIT_FAILURE);
    }
}
```
6. Loop untuk mengirim pesan ke server secara terus menerus yang mana loop tersebut hanya akan berhenti apabila user mengetikkan "keluar" pada input atau apabila terdapat kesalahan dalam mengirim pesan ke server.

7. `close(sock)` : Menutup soket setelah selesai digunakan

### Output

Server saat terhubung dengan 2 client:

![Screenshot_2023-11-04_164705](/uploads/f1755fcf19921e4d8c45d83749d0ec91/Screenshot_2023-11-04_164705.png)

Server saat terhubung dengan 4 client:

![Screenshot_2023-11-04_164751](/uploads/b56a9e9053a77c661c44cddfdb71b820/Screenshot_2023-11-04_164751.png)

Server saat terhubung dengan 6 client:

![Screenshot_2023-11-04_164857](/uploads/04719e73ec43520b8fc41c8e29f7984d/Screenshot_2023-11-04_164857.png)

`samuelyuma` adalah client kelima dan `yumii` adalah client keenam yang mana client keenam tidak dapat mengirimkan pesan ke server karena jumlah client yang dapat terhubung pada server dibatasi hanya 5 client saja

