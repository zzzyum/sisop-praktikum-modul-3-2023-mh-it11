#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define MAX_MESSAGE_SIZE 100
#define MESSAGE_TYPE 1

struct pesan {
    long mtype;
    char mtext[MAX_MESSAGE_SIZE];
};

void decodeBase64(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    int length = BIO_read(bio, output, strlen(input));
    output[length] = '\0';

    BIO_free_all(bio);
}

void showCredentials(FILE *file, FILE *outputFile) {
    char line[MAX_MESSAGE_SIZE];
    while (fgets(line, sizeof(line), file)) {
        char *username = strtok(line, ":");
        char *encryptedPassword = strtok(NULL, "\n");

        char decryptedPassword[MAX_MESSAGE_SIZE];
        decodeBase64(encryptedPassword, decryptedPassword);

        printf("Username: %s, Password: %s\n", username, decryptedPassword);

        fprintf(outputFile, "Username: %s, Password: %s\n", username, decryptedPassword);
    }
}

int authUser(const char *username, const char *password, FILE *file) {
    char line[MAX_MESSAGE_SIZE];
    char storedUsername[MAX_MESSAGE_SIZE];
    char storedDecryptedPassword[MAX_MESSAGE_SIZE];

    while (fgets(line, sizeof(line), file)) {
        sscanf(line, "Username: %[^,], Password: %[^\n]", storedUsername, storedDecryptedPassword);

        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedDecryptedPassword) == 0) {
            return 1;
        }
    }

    return 0;
}

int main() {
    key_t key = ftok(".", 'A');
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    struct pesan msg;

    while (1) {
        if (msgrcv(msgid, &msg, sizeof(msg.mtext), MESSAGE_TYPE, 0) == -1) {
            perror("msgrcv");
            exit(EXIT_FAILURE);
        }

        if (strncmp(msg.mtext, "AUTH:", 5) == 0) {
            char *authCommand = msg.mtext + 5;

            while (*authCommand == ' ') {
                ++authCommand;
            }

            char *username = strtok(authCommand, " ");
            char *password = strtok(NULL, "\n");

            if (password != NULL) {
                char *end = password + strlen(password) - 1;
                while (*end == ' ') {
                    *end-- = '\0';
                }
            }

            FILE *file = fopen("users/decrypted.txt", "r");
            if (!file) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }

            if (authUser(username, password, file)) {
                printf("\nAuthentication successful\n");
            } else {
                printf("\nAuthentication failed\n");
                fclose(file);
                break;
            }

            fclose(file);
        } else if (strcmp(msg.mtext, "CREDS\n") == 0) {
            FILE *file = fopen("users/users.txt", "r");
            if (!file) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }

            FILE *outputFile = fopen("users/decrypted.txt", "w");
            if (!outputFile) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }

            showCredentials(file, outputFile);

            fclose(file);
            fclose(outputFile);
        }
    }

    return 0;
}
