#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>

#define PORT 5000
#define MAX_CLIENTS 5

void *clientHandler(void *socketDescription) {
    int *klien = (int *)socketDescription;
    char nama[128];
    char pesan[4096];

    recv(*klien, nama, sizeof(nama), 0);

    while (1) {
        if (recv(*klien, pesan, sizeof(pesan), 0) <= 0) {
            printf("\nKoneksi dengan klien '%s' terputus!\n\n", nama);
            break;
        }

        printf("%s: %s", nama, pesan);
        memset(pesan, 0, sizeof(pesan));
    }

    close(*klien);
    free(klien);
    pthread_exit(NULL);
}

int main() {
    int server_fd, newSocket, c;
    struct sockaddr_in serverAddress, clientAddress;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Terdapat kesalahan dalam membuat socket.");
        exit(EXIT_FAILURE);
    }

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Proses bind gagal.");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, MAX_CLIENTS) < 0) {
        perror("Terdapat kesalahan saat menghubungkan server dengan klien.");
        exit(EXIT_FAILURE);
    }

    printf("Server terhubung dengan port %d\n", PORT);
    printf("Menunggu klien terhubung dengan server.\n\n");

    c = sizeof(struct sockaddr_in);

    pthread_t client_threads[MAX_CLIENTS];

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        if ((newSocket = accept(server_fd, (struct sockaddr *)&clientAddress, (socklen_t *)&c))) {
            int *clientSocket = malloc(sizeof(int));
            *clientSocket = newSocket;

            if (pthread_create(&client_threads[i], NULL, clientHandler, (void *)clientSocket) < 0) {
                perror("Tidak dapat membuat thread");
                return 1;
            }
        } else {
            perror("Gagal menghubungkan server dengan klien.");
            return 1;
        }
    }

    for (int i = 0; i < MAX_CLIENTS; ++i) {
        pthread_join(client_threads[i], NULL);
    }

    close(server_fd);
    return 0;
}
