#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 5000

int main() {
    int sock = 0;
    struct sockaddr_in serverAddress;
    char nama[128];
    char pesan[4096];

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Terdapat kesalahan saat membuat socket.");
        exit(EXIT_FAILURE);
    }

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0) {
        perror("Alamat IP salah / tidak didukung.");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Gagal terhubung dengan server.");
        exit(EXIT_FAILURE);
    }

    printf("Masukkan nama: ");
    fgets(nama, sizeof(nama), stdin);
    nama[strcspn(nama, "\n")] = 0;

    if (send(sock, nama, sizeof(nama), 0) == -1) {
        perror("Gagal mengirim 'nama' ke server");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("Masukkan pesan: ");
        fgets(pesan, sizeof(pesan), stdin);

        if (strcmp(pesan, "keluar\n") == 0) {
            break;
        }

        if (send(sock, pesan, sizeof(pesan), 0) == -1) {
            perror("Gagal mengirim 'pesan' ke server");
            exit(EXIT_FAILURE);
        }
    }

    close(sock);
    return 0;
}
