#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h> // Sertakan header time.h

unsigned long long factorial(int n) {
    if (n == 0 || n == 1)
        return 1;

    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }

    return result;
}

struct ThreadInfo {
    int value;
    unsigned long long factorial_value;
};

void *calculateFactorial(void *arg) {
    struct ThreadInfo *info = (struct ThreadInfo *)arg;
    info->factorial_value = factorial(info->value);
    return NULL;
}

int main() {
    key_t key = ftok("shared_memory_key", 11);
    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    int transposed_result = *shared_result;

    printf("Matriks Hasil Transposisi:\n");
    printf("%d\n", transposed_result);

    pthread_t threads[1][1];

    struct ThreadInfo thread_info[1][1];

    printf("Hasil faktorial dari matriks hasil transposisi:\n");
    clock_t start_time, end_time;
    start_time = clock(); 

    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 1; j++) {
            thread_info[i][j].value = shared_result[i * 1 + j];
            pthread_create(&threads[i][j], NULL, calculateFactorial, &thread_info[i][j]);
        }
    }

    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < 1; j++) {
            pthread_join(threads[i][j], NULL);
            printf("%llu ", thread_info[i][j].factorial_value); 
        }
        printf("\n");
    }

    end_time = clock(); 

    double cpu_time_used = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang dibutuhkan untuk perhitungan faktorial: %f detik\n", cpu_time_used);

    shmdt(shared_result);

    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
