#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int main() {
    srand(time(NULL));

    int matrix1[1][2] = { {1 + rand() % 4, 1 + rand() % 4} };
    int matrix2[2][1] = { {1 + rand() % 5}, {1 + rand() % 5} };
    int result[1][1] = { {0} };

    result[0][0] = matrix1[0][0] * matrix2[0][0] + matrix1[0][1] * matrix2[1][0];

    result[0][0]--;

    printf("Matriks Pertama:\n");
    printf("%d %d\n", matrix1[0][0], matrix1[0][1]);

    printf("Matriks Kedua:\n");
    printf("%d\n%d\n", matrix2[0][0], matrix2[1][0]);

    printf("Hasil perkalian matriks (dikurangi 1):\n");
    printf("%d\n", result[0][0]);

    key_t key = ftok("shared_memory_key", 11);

    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    *shared_result = result[0][0];

    shmdt(shared_result);

    return 0;
}

