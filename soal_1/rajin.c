#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

clock_t start_time, end_time;
unsigned long long factorial(int n) {
    start_time = clock();
    if (n == 0 || n == 1)
        return 1;

    unsigned long long result = 1;
    for (int i = 2; i <= n; i++) {
        result *= i;
    }
    end_time = clock();

    return result;
}

int main() {
    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int), 0666 | IPC_CREAT);

    if (shmid == -1) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);

    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    int transposed_result = *shared_result;

    printf("Matriks Hasil Transposisi:\n");
    printf("%d\n", transposed_result);

    unsigned long long factorial_value = factorial(transposed_result);


    printf("Hasil faktorial dari matriks hasil transposisi: %llu\n", factorial_value);

    double cpu_time_used = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang dibutuhkan untuk perhitungan faktorial: %f detik\n", cpu_time_used);

    shmdt(shared_result);

    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
