#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>

// remove non letter
void removeNonLetter(char *str)
{
    int i = 0, j = 0;
    while (str[i])
    {
        if (isalpha(str[i]) || str[i] == ' ' || str[i] == '\n')
        {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

// itung huruf
void countLetter(const char *str, const char letter, int *count)
{
    *count = 0;

    for (int i = 0; str[i] != '\0'; i++)
    {
        if (isalpha(str[i]) && str[i] == letter)
        {
            (*count)++;
        }
    }
}

// itung kata
void countWord(const char *str, const char *word, int *count)
{
    char read_word[100];
    *count = 0;
    const char *delimiter = " \n";

    char *token = strtok((char *)str, delimiter);

    while (token != NULL)
    {
        removeNonLetter(token);
        if (strcmp(word, token) == 0)
        {
            (*count)++;
        }
        token = strtok(NULL, delimiter);
    }
}

// bikin frekuensi.log
void writeLog(const char *type, const char *item, int count)
{
    FILE *log_file = fopen("frekuensi.log", "a");

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    fprintf(log_file, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n",
            tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec,
            type, (strcmp(type, "KATA") == 0) ? "Kata" : "Huruf", item, count);

    fclose(log_file);
}

// fungsi download
int downloadFile(const char *url, const char *outputFileName)
{
    char wgetCommand[256];
    snprintf(wgetCommand, sizeof(wgetCommand), "wget --quiet --output-document=%s 'https://drive.google.com/uc?export=download&id=%s'", outputFileName, url);

    FILE *wgetProcess = popen(wgetCommand, "r");
    if (!wgetProcess) {
        perror("Failed to run wget");
        return 1;
    }

    int status = pclose(wgetProcess);
    if (status == -1) {
        perror("pclose() failed");
        return 1;
    } else if (WEXITSTATUS(status) != 0) {
        fprintf(stderr, "wget failed with exit code %d\n", WEXITSTATUS(status));
        return 1;
    }

    return 0;
}

// fungsi main
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: ./8ballondors -kata <item> or -huruf <item>\n");
        return 1;
    }

    int word_fd[2], letter_fd[2];
    pid_t pid;

    if (pipe(word_fd) == -1 || pipe(letter_fd) == -1)
    {
        perror("Pipe creation failed");
        return 1;
    }

    pid = fork();

    if (pid < 0)
    {
        perror("Fork failed");
        return 1;
    }

    if (pid == 0)
    { // child process
        close(word_fd[0]);
        close(letter_fd[0]);

        int downloadResult = downloadFile("16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW", "lirik.txt");
        if (downloadResult != 0)
        {
            printf("Gagal mengunduh file lirik.txt\n");
            return 1;
        }

        FILE *file = fopen("lirik.txt", "r");

        if (file == NULL) {
            perror("File open failed");
            return 1;
        }

        char *file_contents = NULL;
        size_t file_size = 0;

        fseek(file, 0, SEEK_END);
        file_size = ftell(file);
        fseek(file, 0, SEEK_SET);

        file_contents = malloc(file_size + 1);
        fread(file_contents, sizeof(char), file_size, file);

        file_contents[file_size] = '\0';

        int word_count, letter_count;
        if (strcmp(argv[1], "-kata") == 0)
        {
            if (strlen(argv[2]) == 0)
            {
                printf("Salah\n");
                free(file_contents);
                return 1;
            }
            countWord(file_contents, argv[2], &word_count);
            write(word_fd[1], &word_count, sizeof(word_count));
        }
        else if (strcmp(argv[1], "-huruf") == 0)
        {
            if (strlen(argv[2]) != 1)
            {
                printf("Salah\n");
                free(file_contents);
                return 1;
            }
            countLetter(file_contents, argv[2][0], &letter_count);
            write(letter_fd[1], &letter_count, sizeof(letter_count));
        }
        else
        {
            printf("Salah\n");
            free(file_contents);
            return 1;
        }

        free(file_contents);
        close(word_fd[1]);
        close(letter_fd[1]);
        exit(0);
    }
    else
    { // parent process
        close(word_fd[1]);
        close(letter_fd[1]);

        FILE *input_file = fopen("lirik.txt", "r");
        FILE *output_file = fopen("thebeatles.txt", "w");

        char str[100];

        while (fgets(str, 100, input_file) != NULL)
        {
            removeNonLetter(str);
            fputs(str, output_file);
        }

        fclose(input_file);
        fclose(output_file);

        int word_count, letter_count;
        read(word_fd[0], &word_count, sizeof(word_count));
        read(letter_fd[0], &letter_count, sizeof(letter_count));

        const char *type = strcmp(argv[1], "-kata") == 0 ? "KATA" : "HURUF";

        writeLog(type, argv[2], type == "KATA" ? word_count : letter_count);

        close(word_fd[0]);
        close(letter_fd[0]);
    }

    return 0;
}
